﻿using System.Collections.Generic;

namespace DB1.AvaliacaoTecnica.RH.Domain
{
    public sealed partial class Candidate
    {
        public class Builder : IBuilder<Candidate>
        {
            public Builder()
            {

            }

            private CandidateId id;
            private IList<TechnologyId> techologies = new List<TechnologyId>();

            public Builder SetId(CandidateId id)
            {
                this.id = id;

                return this;
            }

            public Builder Add(TechnologyId id)
            {
                if (!techologies.Contains(id))
                    techologies.Add(id);

                return this;
            }

            public Candidate Build() => new Candidate(id, techologies);
        }
    }
}
