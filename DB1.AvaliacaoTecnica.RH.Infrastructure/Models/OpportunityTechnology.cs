﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DB1.AvaliacaoTecnica.RH.Infrastructure.Models
{
    public class OpportunityTechnology
    {
        [Key]
        [ForeignKey(nameof(Opportunity))]
        [Column(Order = 1)]
        public int OpportunityId { get; set; }

        [Key]
        [ForeignKey(nameof(Technology))]
        [Column(Order = 2)]
        public int TechnologyId { get; set; }

        public decimal Weight { get; set; }

        [Required]
        public virtual Opportunity Opportunity { get; set; }

        [Required]
        public virtual Technology Technology { get; set; }
    }
}
