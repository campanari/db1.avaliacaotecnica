﻿angular.module('db1.avaliacao.tecnica')
    .config(['$routeProvider', function ($routeProvider, $routeParams) {

    $routeProvider.when('/home', {
        controller: 'homeController',
        templateUrl: 'views/home.html',
    });

    $routeProvider.when('/github', {
        controller: 'usersController',
        templateUrl: 'views/github/users.html',
    });

    $routeProvider.when('/github/:login', {
        controller: 'userDetailsController',
        templateUrl: 'views/github/userDetails.html',
    });

    $routeProvider.when('/rh/Technologies', {
        controller: 'technologiesController',
        templateUrl: 'views/rh/technologies/list.html',
    });

    $routeProvider.when('/rh/Technologies/:id', {
        controller: 'technologyDetailsController',
        templateUrl: 'views/rh/technologies/details.html',
    });

    $routeProvider.when('/rh/Opportunities', {
        controller: 'opportunitiesController',
        templateUrl: 'views/rh/opportunities/list.html',
    });

    $routeProvider.when('/rh/Opportunities/:id', {
        controller: 'opportunityDetailsController',
        templateUrl: 'views/rh/opportunities/details.html',
    });

    $routeProvider.when('/rh/Candidates', {
        controller: 'candidatesController',
        templateUrl: 'views/rh/candidates/list.html',
    });

    $routeProvider.when('/rh/Candidates/new/', {
        controller: 'candidatesController',
        templateUrl: 'views/rh/candidates/edit.html',
    });

    $routeProvider.when('/rh/Candidates/:id/edit', {
        controller: 'candidatesController',
        templateUrl: 'views/rh/candidates/edit.html',
    });

    $routeProvider.when('/rh/Candidates/:id', {
        controller: 'candidateDetailsController',
        templateUrl: 'views/rh/candidates/details.html',
    });

    $routeProvider.otherwise({
            redirectTo: '/home'
    });

  }]);