﻿using DB1.AvaliacaoTecnica.RH.Infrastructure.Models;
using System.Data.Entity;

namespace DB1.AvaliacaoTecnica.RH.Infrastructure.Context
{
    public class RHContext : DbContext
    {
        public RHContext() : base("DatabaseConnection")
        {
            //Database.Log += s => System.Diagnostics.Debug.WriteLine(s);
            Configuration.AutoDetectChangesEnabled = true;

            // Necessário porque o EF6 não supporta criar tabelas para SQLite
            Database.SetInitializer<RHContext>(null);
            Database.Initialize(true);
        }

        public DbSet<Candidate> Candidates { get; set; }
        public DbSet<CandidateOpportunity> CandidateOpportunities { get; set; }
        public DbSet<CandidateTechnology> CandidateTechnologies { get; set; }
        public DbSet<Opportunity> Opportunities { get; set; }
        public DbSet<OpportunityTechnology> OpportunityTechnologies { get; set; }
        public DbSet<Technology> Technologies { get; set; }
    }
}
