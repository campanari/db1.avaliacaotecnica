﻿'use strict';
angular.module('db1.avaliacao.tecnica')
    .controller('usersController', ['$scope', 'githubService', function ($scope, githubService) {

    $scope.reposLoaded = false;

    githubService.getUsers()
        .success(function (data) {
            $scope.usersData = data;
        });

    $scope.predicate = '-updated_at';

}]);