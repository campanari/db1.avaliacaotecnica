﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace DB1.AvaliacaoTecnica.RH.Domain.Tests
{
    [TestClass]
    public class CandidateTest
    {
        [TestMethod]
        public void ShouldBuildValidCandidate()
        {
            var id = new CandidateId(1);

            var candidate = new Candidate.Builder()
                .SetId(id)
                .Build();

            Assert.AreEqual(id, candidate.Id);
            Assert.IsNotNull(candidate.Technologies);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ShouldntBuildInvalidCandidate()
        {
            var candidate = new Candidate.Builder()
                .Build();
        }

        [TestMethod]
        public void ShouldKnowSomeTechnologies()
        {
            var candidate = new Candidate.Builder()
                .SetId(new CandidateId(1))
                .Add(new TechnologyId(1))
                .Build();

            Assert.IsTrue(candidate.Technologies.Any());
        }

        [TestMethod]
        public void ShouldToRunForOnlyOneJobOpportunity()
        {
            Assert.Inconclusive();
        }
    }
}
