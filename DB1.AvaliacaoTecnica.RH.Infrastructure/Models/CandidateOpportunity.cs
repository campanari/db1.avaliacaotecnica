﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DB1.AvaliacaoTecnica.RH.Infrastructure.Models
{
    public class CandidateOpportunity
    {
        [Key]
        [ForeignKey(nameof(Candidate))]
        [Column(Order = 1)]
        public int CandidateId { get; set; }

        [Key]
        [ForeignKey(nameof(Opportunity))]
        [Column(Order = 2)]
        public int OpportunityId { get; set; }

        [Required]
        public virtual Candidate Candidate { get; set; }

        [Required]
        public virtual Opportunity Opportunity { get; set; }
    }
}
