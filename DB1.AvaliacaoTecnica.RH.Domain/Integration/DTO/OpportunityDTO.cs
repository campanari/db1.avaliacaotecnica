﻿namespace DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO
{
    public struct OpportunityDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
