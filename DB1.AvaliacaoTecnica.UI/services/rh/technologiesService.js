﻿'use strict';
angular.module('db1.avaliacao.tecnica')
    .factory('technologiesService', ['$http', 'rhServer', function ($http, rhServer) {

    var technologiesServiceFactory = {};

    var _getTechnologies = function () {
        return $http.get(rhServer.url + 'api/Technologies').then(function (results) {
            return results;
        });
    };

    var _deleteTechnology = function (id) {
        return $http.delete(rhServer.url + 'api/Technologies/' + id).then(function (results) {
            return results;
        });
    };

    var _createTechnology = function (c) {
        return $http.post(rhServer.url + 'api/Technologies', c, { headers: { 'Content-Type': 'application/json' } });
    };

    var _alterTechnology = function (c) {
        return $http.patch(rhServer.url + 'api/Technologies', c, { headers: { 'Content-Type': 'application/json' } });
    };

    technologiesServiceFactory.getTechnologies = _getTechnologies;
    technologiesServiceFactory.deleteTechnology = _deleteTechnology;
    technologiesServiceFactory.createTechnology = _createTechnology;
    technologiesServiceFactory.alterTechnology = _alterTechnology;

    return technologiesServiceFactory;

}]);