﻿namespace DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO
{
    public struct CandidateTechnologyDTO
    {
        public int CandidateId { get; set; }

        public int TechnologyId { get; set; }
    }
}
