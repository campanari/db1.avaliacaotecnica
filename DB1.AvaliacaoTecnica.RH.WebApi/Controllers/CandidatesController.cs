﻿using DB1.AvaliacaoTecnica.RH.Domain.Integration.Applications;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace DB1.AvaliacaoTecnica.RH.WebApi.Controllers
{
    public class CandidatesController : ApiController
    {
        public CandidatesController(ICandidateAppService service)
        {
            this.service = service;
        }

        private readonly ICandidateAppService service;

        // GET: api/Candidates
        public IQueryable<CandidateDTO> GetCandidates()
        {
            return service.GetCandidates().AsQueryable();
        }

        // GET: api/Candidates/5
        [ResponseType(typeof(CandidateDTO))]
        public IHttpActionResult GetCandidate(int id)
        {
            CandidateDTO candidate = service.GetCandidate(id);

            if (candidate.Id == 0)
                return NotFound();

            return Ok(candidate);
        }

        // PUT: api/Candidates/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCandidate(int id, CandidateDTO candidate)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (id != candidate.Id)
                return BadRequest();

            service.SaveCandidate(candidate);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Candidates
        [ResponseType(typeof(CandidateDTO))]
        public IHttpActionResult PostCandidate(CandidateDTO candidate)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            service.SaveCandidate(candidate);

            return CreatedAtRoute("DefaultApi", new { id = candidate.Id }, candidate);
        }

        // DELETE: api/Candidates/5
        [ResponseType(typeof(CandidateDTO))]
        public IHttpActionResult DeleteCandidate(int id)
        {
            CandidateDTO candidate = service.GetCandidate(id);

            if (candidate.Id == 0)
                return NotFound();

            service.RemoveCandidate(candidate);

            return Ok(candidate);
        }
    }
}