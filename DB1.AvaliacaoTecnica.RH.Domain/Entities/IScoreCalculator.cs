﻿using System.Collections.Generic;

namespace DB1.AvaliacaoTecnica.RH.Domain
{
    public interface IScoreCalculator
    {
        decimal Calculate(IEnumerable<TechnologyId> technologies);
    }
}
