﻿using DB1.AvaliacaoTecnica.RH.Domain.Integration.Repositories;
using DB1.AvaliacaoTecnica.RH.Infrastructure.Context;

namespace DB1.AvaliacaoTecnica.RH.Infrastructure.Repositories
{
    public class UnitOfWorkImpl : IUnitOfWork
    {
        private RHContext db = new RHContext();

        public object Context => db;

        public void Commit() => db.SaveChanges();

        public void RollBack()
        {
            db.Dispose();

            db = new RHContext();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                    db.Dispose();

                disposedValue = true;
            }
        }

        public void Dispose() => Dispose(true);
        #endregion


    }
}
