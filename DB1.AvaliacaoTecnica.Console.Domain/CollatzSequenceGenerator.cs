﻿using System;
using System.Collections.Generic;

namespace DB1.AvaliacaoTecnica.Console.Domain
{
    /// <summary>
    /// Represents a number generator, which produces a sequence of numbers
    /// that obey the Collatz conjecture.
    /// </summary>
    /// <see cref="https://en.wikipedia.org/wiki/Collatz_conjecture"/>
    public sealed class CollatzSequenceGenerator : ISequenceGenerator<long>
    {
        /// <summary>
        /// Generates a sequence of integral numbers obeying the Collatz
        /// conjecture.
        /// </summary>
        /// <param name="seed">
        /// A number used to calculate a starting value for the Collatz's
        /// number sequence.
        /// If a number bellow of 2 is specified, returns an empty enumerable.
        /// </param>
        /// <returns>
        /// An enumerator that can be used to iterate through the generated sequence.
        /// </returns>
        /// <exception cref="OverflowException"/>
        public IEnumerable<long> Generate(long seed)
        {
            if (seed < 2)
                yield break;

            yield return seed;

            do
            {
                yield return seed = Next(seed);
            }
            while (seed > 1);

            // if the seed is below of zero indicates that occured a numeric
            // overflow
            if (seed < 0)
                throw new OverflowException();
        }

        // Generate next number
        private long Next(long number)
        {
            // f(n) = | n ∕ 2     if n ≡ 0 (mod 2) 
            //        | 3 ∙ n + 1 if n ≡ 1 (mod 2)
            return number.IsEven()
                ? number / 2
                : 3 * number + 1;
        }
    }
}
