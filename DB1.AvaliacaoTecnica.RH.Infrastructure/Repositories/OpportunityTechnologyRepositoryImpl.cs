﻿using DB1.AvaliacaoTecnica.RH.Domain;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.Repositories;
using DB1.AvaliacaoTecnica.RH.Infrastructure.Context;
using System.Collections.Generic;

namespace DB1.AvaliacaoTecnica.RH.Infrastructure.Repositories
{
    public class OpportunityTechnologyRepositoryImpl : IOpportunityTechnologyRepository
    {
        public OpportunityTechnologyRepositoryImpl(IUnitOfWork uow)
        {
            this.uow = uow;
        }

        private IUnitOfWork uow;
        private RHContext Context => (RHContext)uow.Context;

        OpportunityTechnologyDTO IRepository<OpportunityTechnologyDTO, OpportunityTechnologyId>.this[OpportunityTechnologyId id]
        {
            get
            {
                var opportunityTechnology = Context.OpportunityTechnologies.Find(id.OpportunityId, id.TechnologyId);

                return opportunityTechnology == null
                    ? default(OpportunityTechnologyDTO)
                    : new OpportunityTechnologyDTO { OpportunityId = opportunityTechnology.OpportunityId, TechnologyId = opportunityTechnology.TechnologyId, Weight = opportunityTechnology.Weight };
            }

            set
            {
            }
        }

        IEnumerable<OpportunityTechnologyDTO> IRepository<OpportunityTechnologyDTO, OpportunityTechnologyId>.GetAll()
        {
            //return Context.OpportunityTechnologies.Select(c => Mapper.Map<Models.OpportunityTechnology, OpportunityTechnologyDTO>(c));

            var opportunityTechnologies = Context.OpportunityTechnologies;

            foreach (var opportunityTechnology in opportunityTechnologies)
                yield return new OpportunityTechnologyDTO { OpportunityId = opportunityTechnology.OpportunityId, TechnologyId = opportunityTechnology.TechnologyId, Weight = opportunityTechnology.Weight };
        }

        void IRepository<OpportunityTechnologyDTO, OpportunityTechnologyId>.Add(OpportunityTechnologyDTO value)
        {
            var entity = Context.OpportunityTechnologies.Create();

            entity.OpportunityId = value.OpportunityId;
            entity.TechnologyId = value.TechnologyId;

            Context.OpportunityTechnologies.Add(entity);
        }

        void IRepository<OpportunityTechnologyDTO, OpportunityTechnologyId>.Remove(OpportunityTechnologyId id)
        {
            var opportunityTechnology = Context.OpportunityTechnologies.Find(id.OpportunityId, id.TechnologyId);

            if (opportunityTechnology != null)
                Context.OpportunityTechnologies.Remove(opportunityTechnology);
        }
    }
}
