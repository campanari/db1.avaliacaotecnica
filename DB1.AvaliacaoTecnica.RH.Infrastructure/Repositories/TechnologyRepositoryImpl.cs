﻿using DB1.AvaliacaoTecnica.RH.Domain;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.Repositories;
using DB1.AvaliacaoTecnica.RH.Infrastructure.Context;
using System.Collections.Generic;

namespace DB1.AvaliacaoTecnica.RH.Infrastructure.Repositories
{
    public class TechnologyRepositoryImpl : ITechnologyRepository
    {
        public TechnologyRepositoryImpl(IUnitOfWork uow)
        {
            this.uow = uow;
        }

        private IUnitOfWork uow;
        private RHContext Context => (RHContext)uow.Context;

        TechnologyDTO IRepository<TechnologyDTO, TechnologyId>.this[TechnologyId id]
        {
            get
            {
                var technology = Context.Technologies.Find(id.Id);

                return technology == null
                    ? default(TechnologyDTO)
                    : new TechnologyDTO { Id = technology.Id, Name = technology.Name };
            }

            set
            {
                if (value.Id != id.Id)
                    return;

                var technology = Context.Technologies.Find(id.Id);

                if (technology != null)
                    Context.Technologies.Remove(technology);

                technology.Name = value.Name;
            }
        }

        IEnumerable<TechnologyDTO> IRepository<TechnologyDTO, TechnologyId>.GetAll()
        {
            //return Context.Technologies.Select(c => Mapper.Map<Models.Technology, TechnologyDTO>(c));

            var technologies = Context.Technologies;

            foreach (var technology in technologies)
                yield return new TechnologyDTO { Id = technology.Id, Name = technology.Name };
        }

        void IRepository<TechnologyDTO, TechnologyId>.Add(TechnologyDTO value)
        {
            var entity = Context.Technologies.Create();

            entity.Name = value.Name;

            Context.Technologies.Add(entity);
        }

        void IRepository<TechnologyDTO, TechnologyId>.Remove(TechnologyId id)
        {
            var technology = Context.Technologies.Find(id.Id);

            if (technology != null)
                Context.Technologies.Remove(technology);
        }
    }
}
