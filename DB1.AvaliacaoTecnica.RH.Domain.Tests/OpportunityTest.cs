﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace DB1.AvaliacaoTecnica.RH.Domain.Tests
{
    [TestClass]
    public class OpportunityTest
    {
        [TestMethod]
        public void ShouldBuildValidOpportunity()
        {
            var id = new OpportunityId(1);

            var opportunity = new Opportunity.Builder()
                .SetId(id)
                .Build();

            Assert.AreEqual(id, opportunity.Id);
            Assert.IsNotNull(opportunity.Calculator);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ShouldntBuildInvalidOpportunity()
        {
            var candidate = new Opportunity.Builder()
             .Build();
        }

        [TestMethod]
        public void ShouldAssociateTechnologiesWithWeight()
        {
            var technologies = Enumerable.Range(1, 10)
                .Select(n => new TechnologyId(n));

            var builder = new WeightedScoreCalculator.Builder();

            foreach (var technology in technologies)
                builder.Add(new TechnologyId(technology.Id), technology.Id);

            var calculator = builder.Build();

            Assert.IsNotNull(calculator);
            Assert.IsInstanceOfType(calculator, typeof(WeightedScoreCalculator));

            var id = new OpportunityId(1);

            var opportunity = new Opportunity.Builder()
                .SetId(id)
                .SetCalculator(calculator)
                .Build();

            Assert.IsNotNull(opportunity);
            Assert.IsNotNull(opportunity.Calculator);
            Assert.IsInstanceOfType(opportunity.Calculator, typeof(WeightedScoreCalculator));

            Assert.AreSame(opportunity.Calculator, calculator);
        }

        [TestMethod]
        public void ShouldCalculateCandidateSScore()
        {
            var technologies = Enumerable.Range(1, 10)
                .Select(n => new TechnologyId(n)).ToArray();

            var builder = new WeightedScoreCalculator.Builder();

            foreach (var technology in technologies)
                builder.Add(new TechnologyId(technology.Id), technology.Id);

            var calculator = builder.Build();

            Assert.IsNotNull(calculator);
            Assert.IsInstanceOfType(calculator, typeof(WeightedScoreCalculator));

            var weight = calculator.Calculate(new[] {
                    technologies[0],
                    technologies[1],
                    technologies[2],
                });

            Assert.AreEqual(6m, weight);

            weight = calculator.Calculate(new[] {
                    technologies[2],
                    technologies[5],
                    technologies[9],
                });

            Assert.AreEqual(19m, weight);
        }

        [TestMethod]
        public void ShouldOrderTheCandidatesByTheirScore()
        {
            Assert.Inconclusive();
        }
    }
}
