﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DB1.AvaliacaoTecnica.RH.Infrastructure.Models
{
    public class CandidateTechnology
    {
        [Key]
        [ForeignKey(nameof(Candidate))]
        [Column(Order = 1)]
        public int CandidateId { get; set; }

        [Key]
        [ForeignKey(nameof(Technology))]
        [Column(Order = 2)]
        public int TechnologyId { get; set; }

        [Required]
        public virtual Candidate Candidate { get; set; }

        [Required]
        public virtual Technology Technology { get; set; }
    }
}
