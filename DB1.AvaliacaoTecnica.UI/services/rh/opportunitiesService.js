﻿'use strict';
angular.module('db1.avaliacao.tecnica')
    .factory('opportunitiesService', ['$http', 'rhServer', function ($http, rhServer) {

    var opportunitiesServiceFactory = {};

    var _getOpportunities = function () {

        return $http.get(rhServer.url + 'api/Opportunities').then(function (results) {
            return results;
        });
    };

    var _getOpportunity = function (id) {

        return $http.get(rhServer.url + 'api/Opportunities/' + id).then(function (results) {
            return results;
        });
    };

    var _deleteOpportunity = function (id) {
        return $http.delete(rhServer.url + 'api/Opportunities/' + id).then(function (results) {
            return results;
        });
    };

    var _createOpportunity = function (c) {
        return $http.post(rhServer.url + 'api/Opportunities', c, { headers: { 'Content-Type': 'application/json' } });
    };

    var _alterOpportunity = function (c) {
        return $http.patch(rhServer.url + 'api/Opportunities', c, { headers: { 'Content-Type': 'application/json' } });
    };

    var _getCandidates = function (id) {

        return $http.get(rhServer.url + 'api/Opportunities/' + id + '/Candidates').then(function (results) {
            return results;
        });
    };

    var _getTechnologies = function (id) {

        return $http.get(rhServer.url + 'api/Opportunities/' + id + '/Technologies').then(function (results) {
            return results;
        });
    };


    opportunitiesServiceFactory.getOpportunities = _getOpportunities;
    opportunitiesServiceFactory.getOpportunity = _getOpportunity;
    opportunitiesServiceFactory.deleteOpportunity = _deleteOpportunity;
    opportunitiesServiceFactory.createOpportunity = _createOpportunity;
    opportunitiesServiceFactory.alterOpportunity = _alterOpportunity;
    opportunitiesServiceFactory.getCandidates = _getCandidates;
    opportunitiesServiceFactory.getTechnologies = _getTechnologies;

    return opportunitiesServiceFactory;

}]);