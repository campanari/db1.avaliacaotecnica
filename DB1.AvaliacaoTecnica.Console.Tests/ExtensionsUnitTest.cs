﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DB1.AvaliacaoTecnica.Console.Domain.Tests
{
    [TestClass]
    public class ExtensionsUnitTest
    {
        [TestMethod]
        public void ShouldDetremineIfANumerIsEven()
        {
            var result = Extensions.IsEven(0);

            Assert.IsTrue(result);

            result = Extensions.IsEven(-1);

            Assert.IsFalse(result);

            result = Extensions.IsEven(-2);

            Assert.IsTrue(result);

            result = Extensions.IsEven(1);

            Assert.IsFalse(result);

            result = Extensions.IsEven(2);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ShouldDetremineIfANumerIsOdd()
        {
            var result = Extensions.IsOdd(0);

            Assert.IsFalse(result);

            result = Extensions.IsOdd(-1);

            Assert.IsTrue(result);

            result = Extensions.IsOdd(-2);

            Assert.IsFalse(result);

            result = Extensions.IsOdd(1);

            Assert.IsTrue(result);

            result = Extensions.IsOdd(2);

            Assert.IsFalse(result);
        }
    }
}
