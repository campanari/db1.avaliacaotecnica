﻿using DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO;

namespace DB1.AvaliacaoTecnica.RH.Domain.Integration.Repositories
{
    public interface ICandidateRepository : IRepository<CandidateDTO, CandidateId>
    {

    }
}
