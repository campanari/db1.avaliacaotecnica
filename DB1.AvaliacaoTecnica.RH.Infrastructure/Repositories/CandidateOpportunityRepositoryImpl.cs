﻿using DB1.AvaliacaoTecnica.RH.Domain;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.Repositories;
using DB1.AvaliacaoTecnica.RH.Infrastructure.Context;
using System.Collections.Generic;

namespace DB1.AvaliacaoTecnica.RH.Infrastructure.Repositories
{
    public class CandidateOpportunityRepositoryImpl : ICandidateOpportunityRepository
    {
        public CandidateOpportunityRepositoryImpl(IUnitOfWork uow)
        {
            this.uow = uow;
        }

        private IUnitOfWork uow;
        private RHContext Context => (RHContext)uow.Context;

        CandidateOpportunityDTO IRepository<CandidateOpportunityDTO, CandidateOpportunityId>.this[CandidateOpportunityId id]
        {
            get
            {
                var candidateOpportunity = Context.CandidateOpportunities.Find(id.CandidateId, id.OpportunityId);

                return candidateOpportunity == null
                    ? default(CandidateOpportunityDTO)
                    : new CandidateOpportunityDTO { CandidateId = candidateOpportunity.CandidateId, OpportunityId = candidateOpportunity.OpportunityId };
            }

            set
            {
            }
        }

        IEnumerable<CandidateOpportunityDTO> IRepository<CandidateOpportunityDTO, CandidateOpportunityId>.GetAll()
        {
            //return Context.CandidateOpportunities.Select(c => Mapper.Map<Models.CandidateOpportunity, CandidateOpportunityDTO>(c));

            var candidateOpportunities = Context.CandidateOpportunities;

            foreach (var candidateOpportunity in candidateOpportunities)
                yield return new CandidateOpportunityDTO { CandidateId = candidateOpportunity.CandidateId, OpportunityId = candidateOpportunity.OpportunityId };
        }

        void IRepository<CandidateOpportunityDTO, CandidateOpportunityId>.Add(CandidateOpportunityDTO value)
        {
            var entity = Context.CandidateOpportunities.Create();

            entity.CandidateId = value.CandidateId;
            entity.OpportunityId = value.OpportunityId;

            Context.CandidateOpportunities.Add(entity);
        }

        void IRepository<CandidateOpportunityDTO, CandidateOpportunityId>.Remove(CandidateOpportunityId id)
        {
            var candidateOpportunity = Context.CandidateOpportunities.Find(id.CandidateId, id.OpportunityId);

            if (candidateOpportunity != null)
                Context.CandidateOpportunities.Remove(candidateOpportunity);
        }
    }
}
