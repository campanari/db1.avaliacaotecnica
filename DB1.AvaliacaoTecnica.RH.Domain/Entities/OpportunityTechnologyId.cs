﻿namespace DB1.AvaliacaoTecnica.RH.Domain
{
    public struct OpportunityTechnologyId
    {
        public readonly int OpportunityId;
        public readonly int TechnologyId;

        public OpportunityTechnologyId(int candidateId, int technologyId)
        {
            OpportunityId = candidateId;
            TechnologyId = technologyId;
        }

        public static bool operator ==(OpportunityTechnologyId x, OpportunityTechnologyId y)
        {
            return x.OpportunityId == y.OpportunityId
                && x.TechnologyId == y.TechnologyId;
        }

        public static bool operator !=(OpportunityTechnologyId x, OpportunityTechnologyId y)
        {
            return x.OpportunityId != y.OpportunityId
                || x.TechnologyId != y.TechnologyId;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            if (!(obj is OpportunityTechnologyId))
                return false;

            return OpportunityId == ((OpportunityTechnologyId)obj).OpportunityId;
        }

        public override int GetHashCode()
        {
            return OpportunityId.GetHashCode() | TechnologyId.GetHashCode();
        }
    }
}
