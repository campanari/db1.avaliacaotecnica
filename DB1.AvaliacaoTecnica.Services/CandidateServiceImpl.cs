﻿using DB1.AvaliacaoTecnica.RH.Domain;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.Repositories;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.Services;
using System.Linq;

namespace DB1.AvaliacaoTecnica.RH.Services
{
    public sealed class CandidateServiceImpl : ICandidateService
    {
        public CandidateServiceImpl(ICandidateRepository repository, ICandidateTechnologyRepository candidateTechnologyRepository)
        {
            this.repository = repository;
            this.candidateTechnologyRepository = candidateTechnologyRepository;
        }

        private ICandidateRepository repository;
        private ICandidateTechnologyRepository candidateTechnologyRepository;

        Candidate ICandidateService.GetCandidate(int id)
        {
            var candidateId = new CandidateId(id);

            var dto = repository[candidateId];

            var candidateBuilder = new Candidate.Builder()
                .SetId(candidateId);

            var technologiesDTO = candidateTechnologyRepository.GetAll().Where(c => c.CandidateId == id);

            foreach (var t in technologiesDTO)
                candidateBuilder.Add(new TechnologyId(t.TechnologyId));

            return candidateBuilder.Build();
        }
    }
}
