﻿namespace DB1.AvaliacaoTecnica.RH.Domain
{
    interface IBuilder<T>
    {
        T Build();
    }
}
