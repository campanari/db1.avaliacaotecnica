﻿using System.Collections.Generic;

namespace DB1.AvaliacaoTecnica.RH.Domain
{
    public sealed partial class WeightedScoreCalculator
    {
        public sealed class Builder : IBuilder<IScoreCalculator>
        {
            public Builder()
            {

            }

            private IDictionary<TechnologyId, decimal> weights = new Dictionary<TechnologyId, decimal>();

            public Builder Add(TechnologyId id, decimal weight)
            {
                weights[id] = weight;

                return this;
            }

            public Builder Remove(TechnologyId id)
            {
                weights.Remove(id);

                return this;
            }

            public IScoreCalculator Build() => new WeightedScoreCalculator(weights);
        }
    }
}
