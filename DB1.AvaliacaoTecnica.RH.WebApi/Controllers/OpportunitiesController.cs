﻿using DB1.AvaliacaoTecnica.RH.Domain.Integration.Applications;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO;
using DB1.AvaliacaoTecnica.RH.Infrastructure.Context;
using DB1.AvaliacaoTecnica.RH.Infrastructure.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace DB1.AvaliacaoTecnica.RH.WebApi.Controllers
{
    public class OpportunitiesController : ApiController
    {
        public OpportunitiesController(IOpportunityAppService service)
        {
            this.service = service;
        }

        private IOpportunityAppService service;
        private RHContext db = new RHContext();

        // GET: api/Opportunities
        public IQueryable<Opportunity> GetOpportunities()
        {
            return db.Opportunities;
        }

        // GET: api/Opportunities/5
        [ResponseType(typeof(Opportunity))]
        public IHttpActionResult GetOpportunity(int id)
        {
            Opportunity opportunity = db.Opportunities.Find(id);
            if (opportunity == null)
            {
                return NotFound();
            }

            return Ok(opportunity);
        }

        // GET: api/Opportunities/5/Technologies
        [Route("api/Opportunities/{id:int}/Technologies")]
        [HttpGet]
        public IQueryable<Technology> GetOpportunityTechnologies(int id)
        {
            return db.OpportunityTechnologies.Where(o => o.OpportunityId == id).Select(o => o.Technology);
        }

        // GET: api/Opportunities/5/Candidates
        [Route("api/Opportunities/{id:int}/Candidates")]
        [HttpGet]
        public IQueryable<CandidateScoreDTO> GetOpportunityCandidates(int id)
        {
            return service.GetOpportunityRankingDTO(id).Candidates.AsQueryable();
        }

        // PUT: api/Opportunities/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutOpportunity(int id, Opportunity opportunity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != opportunity.Id)
            {
                return BadRequest();
            }

            db.Entry(opportunity).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OpportunityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Opportunities
        [ResponseType(typeof(Opportunity))]
        public IHttpActionResult PostOpportunity(Opportunity opportunity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Opportunities.Add(opportunity);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = opportunity.Id }, opportunity);
        }

        // DELETE: api/Opportunities/5
        [ResponseType(typeof(Opportunity))]
        public IHttpActionResult DeleteOpportunity(int id)
        {
            Opportunity opportunity = db.Opportunities.Find(id);
            if (opportunity == null)
            {
                return NotFound();
            }

            db.Opportunities.Remove(opportunity);
            db.SaveChanges();

            return Ok(opportunity);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OpportunityExists(int id)
        {
            return db.Opportunities.Count(e => e.Id == id) > 0;
        }
    }
}