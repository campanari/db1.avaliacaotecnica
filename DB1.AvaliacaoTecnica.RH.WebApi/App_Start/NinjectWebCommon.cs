namespace DB1.AvaliacaoTecnica.RH.WebApi.App_Start
{
    using Application.Services;
    using Domain.Integration.Applications;
    using Domain.Integration.Repositories;
    using Domain.Integration.Services;
    using Infrastructure.Repositories;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject;
    using Ninject.Web.Common;
    using Services;
    using System;
    using System.Web;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();

            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<ICandidateAppService>().To<CandidateAppServiceImpl>().InRequestScope();
            kernel.Bind<ICandidateService>().To<CandidateServiceImpl>().InRequestScope();
            kernel.Bind<ICandidateRepository>().To<CandidateRepositoryImpl>().InRequestScope();
            kernel.Bind<ICandidateOpportunityRepository>().To<CandidateOpportunityRepositoryImpl>().InRequestScope();
            kernel.Bind<ICandidateTechnologyRepository>().To<CandidateTechnologyRepositoryImpl>().InRequestScope();

            kernel.Bind<IOpportunityAppService>().To<OpportunityAppServiceImpl>().InRequestScope();
            kernel.Bind<IOpportunityService>().To<OpportunityServiceImpl>().InRequestScope();
            kernel.Bind<IOpportunityRepository>().To<OpportunityRepositoryImpl>().InRequestScope();
            kernel.Bind<IOpportunityTechnologyRepository>().To<OpportunityTechnologyRepositoryImpl>().InRequestScope();

            kernel.Bind<ITechnologyRepository>().To<TechnologyRepositoryImpl>().InRequestScope();

            kernel.Bind<IUnitOfWork>().To<UnitOfWorkImpl>().InRequestScope();
        }
    }
}
