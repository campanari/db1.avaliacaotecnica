﻿'use strict';
angular.module('db1.avaliacao.tecnica')
    .controller('opportunityDetailsController', ['$scope', '$routeParams', 'opportunitiesService', function ($scope, $routeParams, opportunitiesService) {

    //console.log($scope);

    var _loadOpportunity = function ($scope, id) {
        opportunitiesService.getOpportunity(id).then(function (results) {
            $scope.opportunityData = results.data;

        }, function (error) {
            alert(error.data.message);
        });

        opportunitiesService.getCandidates(id).then(function (results) {
            $scope.candidatesData = results.data;

        }, function (error) {
            alert(error.data.message);
        });
    };

    _loadOpportunity($scope, $routeParams.id);
}]);