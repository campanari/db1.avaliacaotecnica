﻿'use strict';
angular.module('db1.avaliacao.tecnica')
    .controller('userDetailsController', ['$scope', '$routeParams', 'githubService', function ($scope, $routeParams, githubService) {

    $scope.login = $routeParams.login;

    $scope.reposLoaded = false;

    githubService.getUserDetails($routeParams.login)
        .success(function (data) {
            $scope.userData = data;
        });

    githubService.getUserRepositories($routeParams.login)
        .success(function (data) {
            $scope.userReposData = data;
        });

    $scope.predicate = '-updated_at';

}]);
