﻿'use strict';
angular.module('db1.avaliacao.tecnica')
    .controller('opportunitiesController', ['$scope', 'opportunitiesService', function ($scope, opportunitiesService) {

    var _loadOpportunities = function ($scope) {
        opportunitiesService.getOpportunities().then(function (results) {
            $scope.opportunitiesData = results.data;

        }, function (error) {
            alert(error.data.message);
        });
    };

    _loadOpportunities($scope);
}]);