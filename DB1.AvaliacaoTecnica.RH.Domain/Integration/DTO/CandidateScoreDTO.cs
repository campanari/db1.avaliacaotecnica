﻿namespace DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO
{
    public struct CandidateScoreDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal Score { get; set; }
    }
}
