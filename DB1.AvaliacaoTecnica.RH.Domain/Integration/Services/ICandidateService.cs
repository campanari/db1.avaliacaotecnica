﻿namespace DB1.AvaliacaoTecnica.RH.Domain.Integration.Services
{
    public interface ICandidateService
    {
        Candidate GetCandidate(int id);
    }
}
