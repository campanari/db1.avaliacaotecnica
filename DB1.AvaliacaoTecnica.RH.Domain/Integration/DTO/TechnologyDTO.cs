﻿namespace DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO
{
    public struct TechnologyDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
