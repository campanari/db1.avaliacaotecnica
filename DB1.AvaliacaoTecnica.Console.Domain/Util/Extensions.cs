﻿namespace DB1.AvaliacaoTecnica.Console.Domain
{
    public static class Extensions
    {
        /// <summary>
        /// Validate if number is odd.
        /// </summary>
        /// <param name="number">
        /// Number to be validated.
        /// </param>
        /// <returns>
        /// True if number is odd, False otherwise.
        /// </returns>
        public static bool IsOdd(this long number)
        {
            return number % 2 != 0;
        }

        /// <summary>
        /// Validate if number is even.
        /// </summary>
        /// <param name="number">
        /// Number to be validated.
        /// </param>
        /// <returns>
        /// True if number is even, False otherwise.
        /// </returns>
        public static bool IsEven(this long number)
        {
            return number % 2 == 0;
        }
    }
}
