﻿'use strict';
angular.module('db1.avaliacao.tecnica')
    .factory('githubService', ['$http', 'githubServer', function ($http, githubServer) {

    var githubServiceFactory = {};

    var _getUsers = function () {
        return $http.get(githubServer.url + 'users');
    };

    var _getUserDetails = function (login) {
        return $http.get(githubServer.url + 'users/' + login);
    };

    var _getUserRepositories = function (login) {
        return $http.get(githubServer.url + 'users/' + login + '/repos');
    };

    githubServiceFactory.getUsers = _getUsers;
    githubServiceFactory.getUserDetails = _getUserDetails;
    githubServiceFactory.getUserRepositories = _getUserRepositories;

    return githubServiceFactory;

}]);