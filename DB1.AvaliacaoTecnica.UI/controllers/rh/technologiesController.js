﻿'use strict';
angular.module('db1.avaliacao.tecnica')
    .controller('technologiesController', ['$scope', 'technologiesService', function ($scope, technologiesService) {

        var _loadtechnologies = function ($scope) {

            technologiesService.getTechnologies().then(function (results) {
                $scope.technologiesData = results.data;

            }, function (error) {
                alert(error.data.message);
            });
        };

        _loadtechnologies($scope);
}]);