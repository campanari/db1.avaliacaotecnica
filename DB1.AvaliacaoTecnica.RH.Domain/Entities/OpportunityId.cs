﻿namespace DB1.AvaliacaoTecnica.RH.Domain
{
    public struct OpportunityId
    {
        public readonly int Id;

        public OpportunityId(int id)
        {
            this.Id = id;
        }

        public static bool operator ==(OpportunityId x, OpportunityId y)
        {
            return x.Id == y.Id;
        }

        public static bool operator !=(OpportunityId x, OpportunityId y)
        {
            return x.Id != y.Id;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            if (!(obj is OpportunityId))
                return false;

            return Id == ((OpportunityId)obj).Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
