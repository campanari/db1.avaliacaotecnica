﻿namespace DB1.AvaliacaoTecnica.RH.Domain
{
    public struct CandidateOpportunityId
    {
        public readonly int CandidateId;
        public readonly int OpportunityId;

        public CandidateOpportunityId(int candidateId, int technologyId)
        {
            CandidateId = candidateId;
            OpportunityId = technologyId;
        }

        public static bool operator ==(CandidateOpportunityId x, CandidateOpportunityId y)
        {
            return x.CandidateId == y.CandidateId
                && x.OpportunityId == y.OpportunityId;
        }

        public static bool operator !=(CandidateOpportunityId x, CandidateOpportunityId y)
        {
            return x.CandidateId != y.CandidateId
                || x.OpportunityId != y.OpportunityId;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            if (!(obj is CandidateOpportunityId))
                return false;

            return CandidateId == ((CandidateOpportunityId)obj).CandidateId;
        }

        public override int GetHashCode()
        {
            return CandidateId.GetHashCode() | OpportunityId.GetHashCode();
        }
    }
}
