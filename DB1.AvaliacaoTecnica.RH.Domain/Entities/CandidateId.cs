﻿namespace DB1.AvaliacaoTecnica.RH.Domain
{
    public struct CandidateId
    {
        public readonly int Id;

        public CandidateId(int id)
        {
            this.Id = id;
        }

        public static bool operator ==(CandidateId x, CandidateId y)
        {
            return x.Id == y.Id;
        }

        public static bool operator !=(CandidateId x, CandidateId y)
        {
            return x.Id != y.Id;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            if (!(obj is CandidateId))
                return false;

            return Id == ((CandidateId)obj).Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
