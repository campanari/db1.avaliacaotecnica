﻿'use strict';
angular.module('db1.avaliacao.tecnica')
    .controller('candidatesController', ['$scope', 'candidatesService', function ($scope, candidatesService) {

        var _loadcandidates = function ($scope) {
            candidatesService.getCandidates().then(function (results) {
                $scope.candidatesData = results.data;

            }, function (error) {
                alert(error.data.message);
            });
        };

        _loadcandidates($scope);

        var _delete = function (id) {
            bootbox.confirm("Tem certeza que quer excluir este candidato?", function (r) {
                if (r)
                    candidatesService.deleteCandidate(id).then(function () {
                        _loadcandidates($scope);
                    });
            });
        };

        //$scope.new = _new;
        //$scope.alter = _alter;
        $scope.delete = _delete;
}]);