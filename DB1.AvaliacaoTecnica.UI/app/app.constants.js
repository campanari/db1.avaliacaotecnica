﻿angular.module('db1.avaliacao.tecnica')
    .constant("githubServer", {
        url: "https://api.github.com/"
    });

angular.module('db1.avaliacao.tecnica')
    .constant("rhServer", {
        url: "http://localhost:33571/"
    });