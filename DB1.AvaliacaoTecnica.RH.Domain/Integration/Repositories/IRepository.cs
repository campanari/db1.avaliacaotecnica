﻿using System.Collections.Generic;

namespace DB1.AvaliacaoTecnica.RH.Domain.Integration.Repositories
{
    public interface IRepository<T, TKey>
    {
        T this[TKey id] { get; set; }

        void Add(T entity);

        void Remove(TKey id);

        IEnumerable<T> GetAll();
    }
}
