﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Reflection;

namespace DB1.AvaliacaoTecnica.Console.Domain.Tests
{
    [TestClass]
    public class CollatzSequenceUnitTest
    {
        private CollatzSequenceGenerator sequenceGenerator;

        [TestInitialize]
        public void Setup()
        {
            sequenceGenerator = new CollatzSequenceGenerator();
        }

        [TestMethod]
        public void ShouldReturnHalfWhenEven()
        {
            // Should we test private methods?
            var nextMethod = sequenceGenerator.GetType().GetMethod("Next", BindingFlags.NonPublic | BindingFlags.Instance);

            var next = (long)nextMethod.Invoke(sequenceGenerator, new object[] { 2 });

            Assert.AreEqual(next, 1);

            next = (long)nextMethod.Invoke(sequenceGenerator, new object[] { 4 });

            Assert.AreEqual(next, 2);

            next = (long)nextMethod.Invoke(sequenceGenerator, new object[] { 828932 });

            Assert.AreEqual(next, 414466);
        }

        [TestMethod]
        public void ShouldReturnTiplePlusOneWhenOdd()
        {
            // Should we test private methods?
            var nextMethod = sequenceGenerator.GetType().GetMethod("Next", BindingFlags.NonPublic | BindingFlags.Instance);

            var next = (long)nextMethod.Invoke(sequenceGenerator, new object[] { 3 });

            Assert.AreEqual(next, 10);

            next = (long)nextMethod.Invoke(sequenceGenerator, new object[] { 5 });

            Assert.AreEqual(next, 16);

            next = (long)nextMethod.Invoke(sequenceGenerator, new object[] { 828933 });

            Assert.AreEqual(next, 2486800);
        }

        [TestMethod]
        public void ShouldReturnAnEmptyEnumerableWhenSeedIsInvalid()
        {
            var sequence = sequenceGenerator.Generate(-1);

            Assert.IsFalse(sequence.Any());

            sequence = sequenceGenerator.Generate(0);

            Assert.IsFalse(sequence.Any());

            sequence = sequenceGenerator.Generate(1);

            Assert.IsFalse(sequence.Any());
        }

        [TestMethod]
        [ExpectedException(typeof(OverflowException))]
        public void ShouldThrowAnExceptionWhenOccurANumericException()
        {
            var sequence = sequenceGenerator.Generate(long.MaxValue);

            // Force immediate evaluation of the sequence
            sequence.Count();
        }

        [TestMethod]
        public void ShouldReturnAnValidEnumerable()
        {
            var sequence = sequenceGenerator.Generate(2);

            Assert.AreEqual(2, sequence.Count());

            Assert.IsTrue(new long[] { 2, 1 }.SequenceEqual(sequence));

            sequence = sequenceGenerator.Generate(13);

            Assert.AreEqual(10, sequence.Count());

            Assert.IsTrue(new long[] { 13, 40, 20, 10, 5, 16, 8, 4, 2, 1 }.SequenceEqual(sequence));
        }
    }
}
