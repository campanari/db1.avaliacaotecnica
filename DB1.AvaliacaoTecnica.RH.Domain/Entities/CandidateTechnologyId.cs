﻿namespace DB1.AvaliacaoTecnica.RH.Domain
{
    public struct CandidateTechnologyId
    {
        public readonly int CandidateId;
        public readonly int TechnologyId;

        public CandidateTechnologyId(int candidateId, int technologyId)
        {
            CandidateId = candidateId;
            TechnologyId = technologyId;
        }

        public static bool operator ==(CandidateTechnologyId x, CandidateTechnologyId y)
        {
            return x.CandidateId == y.CandidateId
                && x.TechnologyId == y.TechnologyId;
        }

        public static bool operator !=(CandidateTechnologyId x, CandidateTechnologyId y)
        {
            return x.CandidateId != y.CandidateId
                || x.TechnologyId != y.TechnologyId;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            if (!(obj is CandidateTechnologyId))
                return false;

            return CandidateId == ((CandidateTechnologyId)obj).CandidateId;
        }

        public override int GetHashCode()
        {
            return CandidateId.GetHashCode() | TechnologyId.GetHashCode();
        }
    }
}
