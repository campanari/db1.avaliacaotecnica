﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DB1.AvaliacaoTecnica.RH.Domain
{
    public sealed partial class Candidate
    {
        private Candidate(CandidateId id, IEnumerable<TechnologyId> technologies)
        {
            if (id.Id == 0)
                throw new ArgumentException();

            Id = id;
            Technologies = technologies ?? Enumerable.Empty<TechnologyId>(); ;
        }

        public CandidateId Id { get; }

        public IEnumerable<TechnologyId> Technologies { get; }
    }
}
