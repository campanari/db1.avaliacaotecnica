﻿using DB1.AvaliacaoTecnica.RH.Domain;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.Applications;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.Repositories;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.Services;
using System.Collections.Generic;
using System.Linq;

namespace DB1.AvaliacaoTecnica.RH.Application.Services
{
    public sealed class OpportunityAppServiceImpl : IOpportunityAppService
    {
        public OpportunityAppServiceImpl(IUnitOfWork uow, IOpportunityService service, IOpportunityRepository repository, ICandidateRepository candidateRepository)
        {
            this.uow = uow;
            this.service = service;
            this.repository = repository;
            this.candidateRepository = candidateRepository;
        }

        private IUnitOfWork uow;
        private IOpportunityService service;
        private IOpportunityRepository repository;
        private ICandidateRepository candidateRepository;

        public OpportunityRankingDTO GetOpportunityRankingDTO(int id)
        {
            var opportunity = service.GetOpportunity(id);

            var candidates = service.GetCandidates(id);

            var candiatesList = new List<CandidateScoreDTO>();

            foreach (var c in candidates)
            {
                var temp = candidateRepository[new CandidateId(c.Id.Id)];

                candiatesList.Add(new CandidateScoreDTO
                {
                    Id = temp.Id,
                    Name = temp.Name,
                    Score = opportunity.Calculator.Calculate(c.Technologies)
                });
            }

            var opportunityDTO = repository[new OpportunityId(id)];

            return new OpportunityRankingDTO
            {
                Id = opportunityDTO.Id,
                Name = opportunityDTO.Name,
                Candidates = candiatesList.OrderByDescending(c => c.Score).ToArray()
            };
        }

        //public CandidateDTO GetCandidate(int id)
        //{
        //    return repository[new CandidateId(id)];
        //}

        //public IEnumerable<CandidateDTO> GetCandidates()
        //{
        //    return repository.GetAll();
        //}

        //public void SaveCandidate(CandidateDTO candidate)
        //{
        //    repository.Add(candidate);

        //    uow.Commit();
        //}

        //public void RemoveCandidate(CandidateDTO candidate)
        //{
        //    repository.Remove(new CandidateId(candidate.Id));

        //    uow.Commit();
        //}
    }
}
