﻿namespace DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO
{
    public struct CandidateOpportunityDTO
    {
        public int CandidateId { get; set; }

        public int OpportunityId { get; set; }
    }
}
