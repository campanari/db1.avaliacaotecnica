﻿using DB1.AvaliacaoTecnica.RH.Domain;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.Repositories;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.Services;
using System.Collections.Generic;
using System.Linq;

namespace DB1.AvaliacaoTecnica.RH.Services
{
    public sealed class OpportunityServiceImpl : IOpportunityService
    {
        public OpportunityServiceImpl(IOpportunityRepository repository, IOpportunityTechnologyRepository opportunityTechnologyRepository, ICandidateOpportunityRepository candidateOpportunityRepository, ICandidateService candidateService)
        {
            this.repository = repository;
            this.opportunityTechnologyRepository = opportunityTechnologyRepository;
            this.candidateOpportunityRepository = candidateOpportunityRepository;
            this.candidateService = candidateService;
        }

        private IOpportunityRepository repository;
        private IOpportunityTechnologyRepository opportunityTechnologyRepository;
        private ICandidateOpportunityRepository candidateOpportunityRepository;
        private ICandidateService candidateService;

        public IEnumerable<Candidate> GetCandidates(int opportunityId)
        {
            var candidatesDTO = candidateOpportunityRepository.GetAll().Where(c => c.OpportunityId == opportunityId);

            foreach (var c in candidatesDTO)
                yield return candidateService.GetCandidate(c.CandidateId);
        }

        public Opportunity GetOpportunity(int id)
        {
            var opportunityId = new OpportunityId(id);

            var technoligiesDTO = opportunityTechnologyRepository.GetAll().Where(o => o.OpportunityId == id);

            var calculatorBuilder = new WeightedScoreCalculator.Builder();

            foreach (var t in technoligiesDTO)
                calculatorBuilder.Add(new TechnologyId(t.TechnologyId), t.Weight);

            var calculator = calculatorBuilder.Build();

            var dto = repository[opportunityId];

            return new Opportunity.Builder()
                .SetId(opportunityId)
                .SetCalculator(calculator)
                .Build();
        }
    }
}
