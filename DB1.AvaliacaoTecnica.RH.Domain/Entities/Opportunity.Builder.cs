﻿namespace DB1.AvaliacaoTecnica.RH.Domain
{
    public sealed partial class Opportunity
    {
        public class Builder : IBuilder<Opportunity>
        {
            private OpportunityId id;
            private IScoreCalculator calculator;

            public Builder SetId(OpportunityId id)
            {
                this.id = id;

                return this;
            }

            public Builder SetCalculator(IScoreCalculator calculator)
            {
                this.calculator = calculator;

                return this;
            }

            public Opportunity Build()
            {
                return new Opportunity(id, calculator);
            }
        }
    }
}