﻿using DB1.AvaliacaoTecnica.RH.Domain;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.Applications;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.Repositories;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.Services;
using System.Collections.Generic;
using System.Linq;

namespace DB1.AvaliacaoTecnica.RH.Application.Services
{
    public sealed class CandidateAppServiceImpl : ICandidateAppService
    {
        public CandidateAppServiceImpl(IUnitOfWork uow, ICandidateService service, ICandidateRepository repository, ICandidateOpportunityRepository candidateOpportunityRepository, ICandidateTechnologyRepository candidateTechnologyRepository)
        {
            this.uow = uow;
            this.service = service;
            this.repository = repository;
            this.candidateOpportunityRepository = candidateOpportunityRepository;
            this.candidateTechnologyRepository = candidateTechnologyRepository;
        }

        private IUnitOfWork uow;
        private ICandidateService service;
        private ICandidateRepository repository;
        private ICandidateOpportunityRepository candidateOpportunityRepository;
        private ICandidateTechnologyRepository candidateTechnologyRepository;

        public CandidateDTO GetCandidate(int id)
        {
            return repository[new CandidateId(id)];
        }

        public IEnumerable<CandidateDTO> GetCandidates()
        {
            return repository.GetAll();
        }

        public void SaveCandidate(CandidateDTO candidate)
        {
            try
            {
                repository.Add(candidate);

                uow.Commit();
            }
            catch
            {
                uow.RollBack();
            }
        }

        public void RemoveCandidate(CandidateDTO candidate)
        {
            try
            {
                var opportunities = candidateOpportunityRepository.GetAll().Where(c => c.CandidateId == candidate.Id);

                foreach (var o in opportunities)
                    candidateOpportunityRepository.Remove(new CandidateOpportunityId(o.CandidateId, o.OpportunityId));

                var technologies = candidateTechnologyRepository.GetAll().Where(c => c.CandidateId == candidate.Id);

                foreach (var t in technologies)
                    candidateTechnologyRepository.Remove(new CandidateTechnologyId(t.CandidateId, t.TechnologyId));

                repository.Remove(new CandidateId(candidate.Id));

                uow.Commit();
            }
            catch
            {
                uow.RollBack();
            }
        }
    }
}
