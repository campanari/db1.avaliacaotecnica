﻿namespace DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO
{
    public struct CandidateDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
