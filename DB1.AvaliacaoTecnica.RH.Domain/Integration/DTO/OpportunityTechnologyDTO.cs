﻿namespace DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO
{
    public struct OpportunityTechnologyDTO
    {
        public int OpportunityId { get; set; }

        public int TechnologyId { get; set; }

        public decimal Weight { get; set; }
    }
}
