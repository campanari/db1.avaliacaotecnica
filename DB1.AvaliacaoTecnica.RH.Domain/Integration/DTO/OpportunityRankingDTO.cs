﻿namespace DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO
{
    public struct OpportunityRankingDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public CandidateScoreDTO[] Candidates { get; set; }
    }
}
