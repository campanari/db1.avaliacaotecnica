﻿using DB1.AvaliacaoTecnica.RH.Domain;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.Repositories;
using DB1.AvaliacaoTecnica.RH.Infrastructure.Context;
using System.Collections.Generic;

namespace DB1.AvaliacaoTecnica.RH.Infrastructure.Repositories
{
    public class CandidateRepositoryImpl : ICandidateRepository
    {
        public CandidateRepositoryImpl(IUnitOfWork uow)
        {
            this.uow = uow;
        }

        private IUnitOfWork uow;
        private RHContext Context => (RHContext)uow.Context;

        CandidateDTO IRepository<CandidateDTO, CandidateId>.this[CandidateId id]
        {
            get
            {
                var candidate = Context.Candidates.Find(id.Id);

                return candidate == null
                    ? default(CandidateDTO)
                    : new CandidateDTO { Id = candidate.Id, Name = candidate.Name };
            }

            set
            {
                if (value.Id != id.Id)
                    return;

                var candidate = Context.Candidates.Find(id.Id);

                if (candidate != null)
                    Context.Candidates.Remove(candidate);

                candidate.Name = value.Name;
            }
        }

        IEnumerable<CandidateDTO> IRepository<CandidateDTO, CandidateId>.GetAll()
        {
            //return Context.Candidates.Select(c => Mapper.Map<Models.Candidate, CandidateDTO>(c));

            var candidates = Context.Candidates;

            foreach (var candidate in candidates)
                yield return new CandidateDTO { Id = candidate.Id, Name = candidate.Name };
        }

        void IRepository<CandidateDTO, CandidateId>.Add(CandidateDTO value)
        {
            var entity = Context.Candidates.Create();

            entity.Name = value.Name;

            Context.Candidates.Add(entity);
        }

        void IRepository<CandidateDTO, CandidateId>.Remove(CandidateId id)
        {
            var candidate = Context.Candidates.Find(id.Id);

            if (candidate != null)
                Context.Candidates.Remove(candidate);
        }
    }
}
