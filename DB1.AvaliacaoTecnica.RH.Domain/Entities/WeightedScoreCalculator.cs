﻿using System.Collections.Generic;
using System.Linq;

namespace DB1.AvaliacaoTecnica.RH.Domain
{
    public sealed partial class WeightedScoreCalculator : IScoreCalculator
    {
        private WeightedScoreCalculator(IEnumerable<KeyValuePair<TechnologyId, decimal>> weights)
        {
            this.weights = weights.ToDictionary(w => w.Key, w => w.Value);
        }

        private IDictionary<TechnologyId, decimal> weights;

        public decimal Calculate(IEnumerable<TechnologyId> technologies)
        {
            var score = 0m;

            foreach (var t in technologies)
                if (weights.ContainsKey(t))
                    score += weights[t];

            return score;
        }
    }
}
