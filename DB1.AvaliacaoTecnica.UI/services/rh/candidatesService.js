﻿'use strict';
angular.module('db1.avaliacao.tecnica')
    .factory('candidatesService', ['$http', 'rhServer', function ($http, rhServer) {

    var candidatesServiceFactory = {};

    var _getCandidates = function () {

        return $http.get(rhServer.url + 'api/Candidates').then(function (results) {
            return results;
        });
    };

    var _deleteCandidate = function (id) {
        return $http.delete(rhServer.url + 'api/Candidates/' + id).then(function (results) {
            return results;
        });
    };

    var _createCandidate = function (c) {
        return $http.post(rhServer.url + 'api/Candidates', c, { headers: { 'Content-Type': 'application/json' } });
    };

    var _alterCandidate = function (c) {
        return $http.patch(rhServer.url + 'api/Candidates', c, { headers: { 'Content-Type': 'application/json' } });
    };

    candidatesServiceFactory.getCandidates = _getCandidates;
    candidatesServiceFactory.deleteCandidate = _deleteCandidate;
    candidatesServiceFactory.createCandidate = _createCandidate;
    candidatesServiceFactory.alterCandidate = _alterCandidate;

    return candidatesServiceFactory;

}]);