﻿using System;

namespace DB1.AvaliacaoTecnica.RH.Domain.Integration.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        object Context { get; }

        void Commit();
        void RollBack();
    }
}
