﻿using System.Collections.Generic;
using System.Linq;

namespace DB1.AvaliacaoTecnica.RH.Domain
{
    public sealed partial class SimpleScoreCalculator : IScoreCalculator
    {
        public decimal Calculate(IEnumerable<TechnologyId> technologies)
        {
            return technologies?.Count() ?? 0;
        }
    }
}
