﻿using DB1.AvaliacaoTecnica.RH.Domain;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.Repositories;
using DB1.AvaliacaoTecnica.RH.Infrastructure.Context;
using System.Collections.Generic;

namespace DB1.AvaliacaoTecnica.RH.Infrastructure.Repositories
{
    public class OpportunityRepositoryImpl : IOpportunityRepository
    {
        public OpportunityRepositoryImpl(IUnitOfWork uow)
        {
            this.uow = uow;
        }

        private IUnitOfWork uow;
        private RHContext Context => (RHContext)uow.Context;

        OpportunityDTO IRepository<OpportunityDTO, OpportunityId>.this[OpportunityId id]
        {
            get
            {
                var opportunity = Context.Opportunities.Find(id.Id);

                return opportunity == null
                    ? default(OpportunityDTO)
                    : new OpportunityDTO { Id = opportunity.Id, Name = opportunity.Name };
            }

            set
            {
                if (value.Id != id.Id)
                    return;

                var opportunity = Context.Opportunities.Find(id.Id);

                if (opportunity != null)
                    Context.Opportunities.Remove(opportunity);

                opportunity.Name = value.Name;
            }
        }

        IEnumerable<OpportunityDTO> IRepository<OpportunityDTO, OpportunityId>.GetAll()
        {
            //return Context.Opportunities.Select(c => Mapper.Map<Models.Opportunity, OpportunityDTO>(c));

            var opportunitys = Context.Opportunities;

            foreach (var opportunity in opportunitys)
                yield return new OpportunityDTO { Id = opportunity.Id, Name = opportunity.Name };
        }

        void IRepository<OpportunityDTO, OpportunityId>.Add(OpportunityDTO value)
        {
            var entity = Context.Opportunities.Create();

            entity.Name = value.Name;

            Context.Opportunities.Add(entity);
        }

        void IRepository<OpportunityDTO, OpportunityId>.Remove(OpportunityId id)
        {
            var opportunity = Context.Opportunities.Find(id.Id);

            if (opportunity != null)
                Context.Opportunities.Remove(opportunity);
        }
    }
}
