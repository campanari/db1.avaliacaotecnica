﻿using System.Collections.Generic;

namespace DB1.AvaliacaoTecnica.RH.Domain.Integration.Services
{
    public interface IOpportunityService
    {
        Opportunity GetOpportunity(int id);

        IEnumerable<Candidate> GetCandidates(int opportunityId);
    }
}
