﻿using DB1.AvaliacaoTecnica.RH.Domain;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO;
using DB1.AvaliacaoTecnica.RH.Domain.Integration.Repositories;
using DB1.AvaliacaoTecnica.RH.Infrastructure.Context;
using System.Collections.Generic;

namespace DB1.AvaliacaoTecnica.RH.Infrastructure.Repositories
{
    public class CandidateTechnologyRepositoryImpl : ICandidateTechnologyRepository
    {
        public CandidateTechnologyRepositoryImpl(IUnitOfWork uow)
        {
            this.uow = uow;
        }

        private IUnitOfWork uow;
        private RHContext Context => (RHContext)uow.Context;

        CandidateTechnologyDTO IRepository<CandidateTechnologyDTO, CandidateTechnologyId>.this[CandidateTechnologyId id]
        {
            get
            {
                var candidateTechnology = Context.CandidateTechnologies.Find(id.CandidateId, id.TechnologyId);

                return candidateTechnology == null
                    ? default(CandidateTechnologyDTO)
                    : new CandidateTechnologyDTO { CandidateId = candidateTechnology.CandidateId, TechnologyId = candidateTechnology.TechnologyId };
            }

            set
            {
            }
        }

        IEnumerable<CandidateTechnologyDTO> IRepository<CandidateTechnologyDTO, CandidateTechnologyId>.GetAll()
        {
            //return Context.CandidateTechnologies.Select(c => Mapper.Map<Models.CandidateTechnology, CandidateTechnologyDTO>(c));

            var candidateTechnologies = Context.CandidateTechnologies;

            foreach (var candidateTechnology in candidateTechnologies)
                yield return new CandidateTechnologyDTO { CandidateId = candidateTechnology.CandidateId, TechnologyId = candidateTechnology.TechnologyId };
        }

        void IRepository<CandidateTechnologyDTO, CandidateTechnologyId>.Add(CandidateTechnologyDTO value)
        {
            var entity = Context.CandidateTechnologies.Create();

            entity.CandidateId = value.CandidateId;
            entity.TechnologyId = value.TechnologyId;

            Context.CandidateTechnologies.Add(entity);
        }

        void IRepository<CandidateTechnologyDTO, CandidateTechnologyId>.Remove(CandidateTechnologyId id)
        {
            var candidateTechnology = Context.CandidateTechnologies.Find(id.CandidateId, id.TechnologyId);

            if (candidateTechnology != null)
                Context.CandidateTechnologies.Remove(candidateTechnology);
        }
    }
}
