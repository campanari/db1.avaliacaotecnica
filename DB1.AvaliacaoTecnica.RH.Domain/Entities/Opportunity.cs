﻿using System;

namespace DB1.AvaliacaoTecnica.RH.Domain
{
    public sealed partial class Opportunity
    {
        private Opportunity(OpportunityId id, IScoreCalculator calculator)
        {
            if (id.Id == 0)
                throw new ArgumentException();

            Id = id;
            Calculator = calculator ?? new SimpleScoreCalculator();
        }

        public OpportunityId Id { get; }

        public IScoreCalculator Calculator { get; }
    }
}