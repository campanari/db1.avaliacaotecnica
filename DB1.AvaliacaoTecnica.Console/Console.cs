﻿using DB1.AvaliacaoTecnica.Console.Domain;
using System;
using System.Linq;

namespace DB1.AvaliacaoTecnica.Console
{
    class Console
    {
        static int Main(string[] args)
        {
            System.Console.WriteLine($"Avaliação Técnica - Diego Campanari");
            System.Console.WriteLine($"Pressione uma tecla para iniciar...");
            System.Console.WriteLine();
            System.Console.ReadKey();

            System.Console.WriteLine($"{DateTime.Now:O} - Item 1 - Início");

            var sequence = new CollatzSequenceGenerator();

            var result1 = Enumerable.Range(1, 1000000).AsParallel()
                .Select(n => new { Seed = n, Length = sequence.Generate(n).Count() })
                .OrderByDescending(n => n.Length)
                .FirstOrDefault();

            System.Console.WriteLine($"Número inicial: {result1.Seed} Tamanho: {result1.Length}");

            System.Console.WriteLine($"{DateTime.Now:O} - Item 1 - Fim");
            System.Console.WriteLine();

            System.Console.WriteLine($"{DateTime.Now:O} - Item 2 - Início");

            var numeros = new long[] { 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144 };

            var result2 = numeros.Any(n => n.IsEven());

            System.Console.WriteLine($"[{string.Join(", ", numeros)}] {(result2 ? "não" : "")} possui somente números ímpares!");

            System.Console.WriteLine($"{DateTime.Now:O} - Item 2 - Fim");
            System.Console.WriteLine();

            System.Console.WriteLine($"{DateTime.Now:O} - Item 3 - Início");

            var primeiroArray = new[] { 1, 3, 7, 29, 42, 98, 234, 93 };
            var segundoArray = new[] { 4, 6, 93, 7, 55, 32, 3 };

            var result3 = primeiroArray.Except(segundoArray);

            System.Console.WriteLine($"[{ string.Join(", ", result3)}]");

            System.Console.WriteLine($"{DateTime.Now:O} - Item 3 - Fim");
            System.Console.WriteLine();

            System.Console.WriteLine($"Pressione uma tecla para finalizar...");
            System.Console.ReadKey();

            return 0;
        }
    }
}
