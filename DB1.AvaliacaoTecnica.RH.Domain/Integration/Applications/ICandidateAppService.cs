﻿using DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO;
using System.Collections.Generic;

namespace DB1.AvaliacaoTecnica.RH.Domain.Integration.Applications
{
    public interface ICandidateAppService
    {
        CandidateDTO GetCandidate(int id);
        IEnumerable<CandidateDTO> GetCandidates();
        void SaveCandidate(CandidateDTO candidate);
        void RemoveCandidate(CandidateDTO candidate);
    }
}
