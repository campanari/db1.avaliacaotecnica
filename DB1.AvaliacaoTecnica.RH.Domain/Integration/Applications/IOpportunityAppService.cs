﻿using DB1.AvaliacaoTecnica.RH.Domain.Integration.DTO;

namespace DB1.AvaliacaoTecnica.RH.Domain.Integration.Applications
{
    public interface IOpportunityAppService
    {
        OpportunityRankingDTO GetOpportunityRankingDTO(int id);
    }
}
