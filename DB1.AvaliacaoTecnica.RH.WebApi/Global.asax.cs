﻿using System.Web;
using System.Web.Http;

namespace DB1.AvaliacaoTecnica.RH.WebApi
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
