﻿using System.Collections.Generic;

namespace DB1.AvaliacaoTecnica.Console.Domain
{
    /// <summary>
    /// Representes a sequence generator.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the elements of the sequence.
    /// </typeparam>
    public interface ISequenceGenerator<T>
    {
        /// <summary>
        /// Generate a sequence of type T.
        /// </summary>
        /// <param name="seed">
        /// A value used to calculate a starting value for the sequence.
        /// </param>
        /// <returns>
        /// An enumerator that can be used to iterate through the generated
        /// sequence.
        /// </returns>
        IEnumerable<T> Generate(T seed);
    }
}
