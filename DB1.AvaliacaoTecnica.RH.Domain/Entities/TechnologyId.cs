﻿namespace DB1.AvaliacaoTecnica.RH.Domain
{
    public struct TechnologyId
    {
        public readonly int Id;

        public TechnologyId(int id)
        {
            this.Id = id;
        }

        public static bool operator ==(TechnologyId x, TechnologyId y)
        {
            return x.Id == y.Id;
        }

        public static bool operator !=(TechnologyId x, TechnologyId y)
        {
            return x.Id != y.Id;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            if (!(obj is TechnologyId))
                return false;

            return Id == ((TechnologyId)obj).Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
